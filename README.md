# **🚀 Create CloudFront with S3 bucket 🚀**

This plugin will be able to create a Cloudfront pointing to S3 domain origin.

## **✅ Installation**

```bash
npm i create-cloudfront-s3
```

## **✅ How to use**

-   You need to configure the .awsconfig file. In this step you can change the parameters of the script.

-   Then, you need to place the .awsconfig file in the root directory of your project.

-   Then, you need to put your amazon keys as development variables.

    #### Cli

    ```bash
    AWS_SECRET_ACCESS_SECRET_KEY YOUR_API_ACCESS_KEY
    ```

    ```bash
    AWS_ACCESS_KEY_ID YOUR_API_ACCESS_KEY
    ```

    #### Fish

    ```bash
    set -gx AWS_SECRET_ACCESS_SECRET_KEY YOUR_API_ACCESS_KEY
    ```

    ```bash
    set -gx AWS_ACCESS_KEY_ID YOUR_API_ACCESS_KEY
    ```

-   To finish you need to place the function to be able to create the CND and the S3 Bucket .

    ```js
    const ARAWS = require('../lib/core.js');

    ARAWS.createCDNWithS3('YOUR_BUCKETNAME', (result) => {
      console.log('result', result );
    });
    ```

##### **💻 Created by Group AR 💻**
