module.exports = {
  awsconfig: {
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
  },
  s3Bucket: (bucketId = null) => {
    return {
      Cors: {
        Bucket: bucketId ? bucketId : 'YOUR_BUCKET_ID',
        CORSConfiguration: {
          CORSRules: [{
            AllowedMethods: [
              'GET', 'POST'
            ],
            AllowedOrigins: [
              'http://localhost:3000',
            ],
            AllowedHeaders: [
              '*',
            ],
          }]
        },
      }
    }
  },
  cloudFrontParams: (BucketId = null) => {
    return {
      DistributionConfig: {
        CallerReference: BucketId ? BucketId : 'YOUR BUCKET-REFERENCE',
        Aliases: {
          Quantity: 0
        },
        DefaultRootObject: '',
        Origins: {
          Quantity: 1,
          Items: [{
            Id: BucketId ? BucketId : 'YOUR-BUCKET-ID',
            DomainName: BucketId ? `${BucketId}.s3.amazonaws.com` : 'YOUR-BUCKET-DOMAIN',
            S3OriginConfig: {
              OriginAccessIdentity: ''
            }
          }]
        },
        DefaultCacheBehavior: {
          TargetOriginId: BucketId ? BucketId : 'YOUR-BUCKET-ID',
          ForwardedValues: {
            QueryString: true,
            Cookies: {
              Forward: 'none'
            },
            Headers: {
              Quantity: 1,
              Items: [
                'Origin',
              ]
            },
          },
          TrustedSigners: {
            Enabled: false,
            Quantity: 0
          },
          ViewerProtocolPolicy: 'redirect-to-https',
          MinTTL: 3600,
          AllowedMethods: {
            Items: [
              'GET', 'HEAD'
            ],
            Quantity: 2,
          },
          Compress: true,
          SmoothStreaming: false
        },
        CacheBehaviors: {
          Quantity: 0
        },
        Comment: '',
        Logging: {
          Enabled: false,
          IncludeCookies: true,
          Bucket: '',
          Prefix: ''
        },
        PriceClass: 'PriceClass_All',
        Enabled: true
      }
    }
  }
};
