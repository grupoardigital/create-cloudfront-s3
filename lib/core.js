const AWS = require('aws-sdk');
const path = require('path');
const awsConfig = require(path.resolve('./.awsconfig'));

AWS.config = awsConfig.awsconfig;
const s3Bucket = new AWS.S3();
const cloudFront = new AWS.CloudFront();

const STATUS_CREATED = 201;

function initS3Bucket(deployName, callback) {
  s3Bucket.listBuckets((err, data) => {
    if (err) return callback({
      status: 500,
      err
    });
    if (data.Buckets.length > 0) {
      const s3Bucket = filterS3BucketName(data.Buckets, deployName);
      ifS3BucketExist(s3Bucket, deployName, (response) => {
        return callback(response);
      });
    } else {
      createS3Bucket(deployName, (response) => {
        console.log(response.msj);
        return callback(response);
      });
    }
  });
}


function getS3BucketObjects(bucketName, callback) {
  const params = {
    Bucket: bucketName,
  };
  s3Bucket.listObjects(params, function(err, data) {
    if (err) return callback({
      status: 500,
      err
    });
    if (data.Contents.length > 0) {
      deleteS3BucketObject(bucketName, data, (response) => {
        return callback(response)
      });
    } else {
      console.log(`-> S3 Bucket${bucketName} files is empty`);
      return callback({
        status: 201,
        msj: `S3 Bucket${bucketName} files is empty`
      });
    }
  });
}

async function deleteS3BucketObject(bucketName, files, callback) {
  let objects = [];
  await Promise.all(files.Contents.map(async (file, index) => {
    objects.push({
      Key: file.Key
    });
  }));
  var params = {
    Bucket: bucketName,
    Delete: {
      Objects: objects,
      Quiet: false
    },
  };
  s3Bucket.deleteObjects(params, function(err, data) {
    console.log(`-> Removing ${files.Contents.length} objects of ${bucketName} S3 buckets`);
    if (err) return callback({
      status: 500,
      err
    });
    else return callback({
      status: 201,
      msj: 'Objects deleted correctly'
    });
  });
}

function ifS3BucketExist(s3Bucket, deployName, callback) {
  if (s3Bucket.length > 0) {
    console.log(`-> S3 Bucket ${deployName} alredy exist`);
    getS3BucketObjects(deployName, (response) => {
      return callback(response);
    });
  } else {
    console.log(`-> Creating a new ${deployName} S3 Bucket`);
    createS3Bucket(deployName, (response) => {
      return callback(response);
    });
  }
}

function filterS3BucketName(s3BucketList, deployName) {
  return s3BucketList.filter((bucket) => {
    return bucket.Name === deployName;
  });
}

function deleteS3Buket(deployName, callback) {
  const params = {
    Bucket: deployName,
  };
  s3Bucket.deleteBucket(params, function(err, data) {
    if (err) {
      return callback({
        status: 500,
        err
      });
    } else {
      createS3Bucket(deployName, (response) => {
        return callback(response);
      });
    }
  });
}

function createS3Bucket(deployName, callback) {
  const params = {
    Bucket: deployName,
  };
  console.log('-> Creating s3 bucket');
  s3Bucket.createBucket(params, (err, data) => {
    if (err) return callback({
      status: 500,
      err
    });
    console.log('-> Adding cors to s3 bucket');
    putBucketCors(deployName, (err, data) => {
      if (err) if (err) return callback({
        status: 500,
        err
      });
    });
    return callback({
      status: 201,
      msj: 'Bucket created'
    });
  });
}

function putBucketCors(bucketName, callback) {
  const params = awsConfig.s3Bucket(bucketName).Cors;
  s3Bucket.putBucketCors(params, callback);
}
// CLOUDFRONT DEPLOY
function initCloudfront(deployName, callback) {
  const params = {
    Marker: '',
    MaxItems: '10'
  };
  console.log('-> Now lets create the distribution, this may take between 10 to 15 minutes');
  cloudFront.listDistributions(params, (err, data) => {
    if (err) return callback({
      status: 500,
      err
    });
    if (data.Items.length > 0) {
      const cloudFront = filterCloudFrontList(data.Items, deployName);
      ifCloundFrontExist(cloudFront, deployName, (response) => {
        return callback(response);
      });
    } else {
      createCloudFrontDistribution(deployName, (response) => {
        return callback(response);
      });
    }
  })
}

function ifCloundFrontExist(cloudFront, deployName, callback) {
  if (cloudFront.length > 0) {
    return callback({
      status: 200,
      msj: `Distribution {${deployName}} alredy exist`,
      cloudFront
    });
  } else {
    createCloudFrontDistribution(deployName, (response) => {
      return callback(response);
    });
  }
}

function filterCloudFrontList(CloudFrontList, deployName) {
  return CloudFrontList.filter((distribution) => {
    return distribution.Origins.Items[0].Id === deployName;
  });
}

function createCloudFrontDistribution(deployName, callback) {
  const cloudFrontParams = awsConfig.cloudFrontParams(deployName);
  cloudFront.createDistribution(cloudFrontParams, (err, data) => { //Delete params if you can use status config
    if (err) return callback({
      status: 500,
      err
    });
    else checkCloudFrontCreateDeploy(data.Distribution.Id, (response) => {
      return callback(response);
    });
  });
}

function checkCloudFrontCreateDeploy(cloudFrontId, callback) {
  const params = {
    Id: cloudFrontId
  };
  cloudFront.waitFor('distributionDeployed', params, function(err, data) {
    if (err) return callback({
      status: 500,
      err
    });
    else return callback({
      status: 201,
      msj: 'Distribution created',
      data
    });
  });
}

function createCDNWithS3(name, callback) {
  initS3Bucket(name, (s3Result) => {
    if (s3Result.status === STATUS_CREATED) {
      initCloudfront(name, (cloudFrontResult) => {
        if (typeof callback === 'function') {
          callback(cloudFrontResult);
        }
      });
    } else {
      console.log(`->${s3Result.status}`);
      console.log(`->${s3Result.err.message}`);
    }
  });
}

exports.initS3Bucket = initS3Bucket;
exports.initCloudfront = initCloudfront;
exports.createCDNWithS3 = createCDNWithS3;
